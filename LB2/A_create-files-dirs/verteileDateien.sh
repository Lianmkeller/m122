#!/bin/bash

base_dir="gen"
mkdir -p "$base_dir"

for klassen_file in _schulklassen/*.txt; do

  klassen_name=$(basename "$klassen_file" .txt)
  klassen_dir="$base_dir/$klassen_name"
  mkdir -p "$klassen_dir"
  while IFS= read -r schueler; do
    schueler_dir="$klassen_dir/$schueler"
    cp _templates/* "$schueler_dir"
  done < "$klassen_file"
done

echo "Dateien wurden erfolgreich verteilt."
