#!/bin/bash


mkdir -p _templates
touch _templates/datei-{1..3}.txt


mkdir -p _schulklassen


declare -A schulklassen
schulklassen["M122-AP22b"]="Rechberger Eugster Keller Meier Müller Schmid Huber Fischer Weber Langer Kellenberger Ruckstuhl"
schulklassen["M122-AP22c"]="Rüesch Schär Camenzind Krämer Malmedie Rossi Vettel Beck Iten Boronka Kaufmann Grüter"
schulklassen["M122-AP22d"]="Schibli Schmidt Schneider Fischer Weber Meyer Wagner Becker Berset Hoffmann Schäfer Koch"


for klasse in "${!schulklassen[@]}"; do
  echo "${schulklassen[$klasse]}" | tr ' ' '\n' > "_schulklassen/$klasse.txt"
done

echo "Vorlagen und Schulklassen-Dateien wurden erfolgreich erstellt."
