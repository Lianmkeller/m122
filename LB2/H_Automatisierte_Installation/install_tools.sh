#!/bin/bash

CONFIG_FILE="config.txt"

if [ -f "$CONFIG_FILE" ]; then
    source "$CONFIG_FILE"
else
    echo "Konfigurationsdatei '$CONFIG_FILE' nicht gefunden."
    exit 1
fi

log_message() {
    local message="$1"
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $message"
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $message" >> install.log
}

log_message "Installation gestartet."

java_version="22"
java_download_url="https://download.oracle.com/java/22/latest/jdk-22_linux-x64_bin.tar.gz"
java_install_dir="/opt/jdk-22"  # Update this with your installation directory

# Function to download Java archive
download_java() {
    log_message "Java $java_version wird heruntergeladen von: $java_download_url"
    wget -q "$java_download_url" -P /tmp/
    if [ $? -ne 0 ]; then
        log_message "Fehler beim Herunterladen von Java $java_version."
        exit 1
    fi
    log_message "Java $java_version erfolgreich heruntergeladen."
}

# Function to extract Java archive
extract_java() {
    log_message "Entpacken von Java $java_version."
    mkdir -p "$java_install_dir"
    tar -xzf /tmp/jdk-22_linux-x64_bin.tar.gz -C "$java_install_dir" --strip-components=1
    if [ $? -ne 0 ]; then
        log_message "Fehler beim Entpacken von Java $java_version."
        exit 1
    fi
}

# Attempt to download and extract Java
download_java
extract_java

# Check if extraction was successful
java -version | tee /dev/tty | grep "$java_version" >> install.log
if [ $? -eq 0 ]; then
    log_message "Java-Installation erfolgreich abgeschlossen."
    echo "Java wurde erfolgreich installiert (Version: $java_version)"
else
    log_message "Java-Installation fehlgeschlagen."
    log_message "Lade Java erneut herunter, ohne das das File anschließend entpackt wird."
    download_java  # Redownload Java without attempting extraction
    log_message "Installation abgebrochen."
fi

log_message "Installation abgeschlossen."
