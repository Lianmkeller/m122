#!/bin/bash


SOURCE_DIR="$1"
BACKUP_DIR="$HOME/backups"
TIMESTAMP=$(date +"%Y%m%d%H%M%S")
BACKUP_NAME="backup-$TIMESTAMP.tar.gz"

if [ -z "$SOURCE_DIR" ]; then
  echo "Bitte geben Sie ein Quellverzeichnis an."
  exit 1
fi

tar -czf "$BACKUP_DIR/$BACKUP_NAME" "$SOURCE_DIR"

if [ $? -eq 0 ]; then
  echo "Backup erfolgreich erstellt: $BACKUP_DIR/$BACKUP_NAME"
fi
exit 0
