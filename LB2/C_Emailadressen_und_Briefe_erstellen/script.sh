#!/bin/bash


class_name="PE23d"
last_name="Keller"


current_date=$(date +"%Y-%m-%d_%H-%M-%S")
output_file="${current_date}_mailimports.csv"
formatted_date=$(date +"%d.%m.%Y")


archive_name="${current_date}_newMailadr_${class_name}_${last_name}.zip"


curl -s https://haraldmueller.ch/schueler/m122_projektunterlagen/b/MOCK_DATA.csv > mock_data.csv


function clean() {
    echo "$1" | tr '[:upper:]' '[:lower:]' | \
    sed -e 'y/äáàâåéèêëìíîïöôüûç/aaaaaeeeeiiiioouuc/' -e 's/[^a-z]//g'
}


echo "GenerierteEmailadresse;GeneriertesPasswort" > "$output_file"


while IFS=, read -r id first_name last_name gender street street_number postal_code city
do
    
    if [[ "$id" == "id" ]]; then
        continue
    fi

    
    clean_first_name=$(clean "$first_name")
    clean_last_name=$(clean "$last_name")


    email_base="${clean_first_name}.${clean_last_name}"
    email="${email_base}@edu.tbz.ch"

    counter=1
    while grep -q "^${email};" "$output_file"; do
        email="${email_base}${counter}@edu.tbz.ch"
        counter=$((counter + 1))
    done


    password=$(< /dev/urandom tr -dc 'a-zA-Z0-9' | head -c8)


    echo "${email};${password}" >> "$output_file"


    if [[ "$gender" == "Male" ]]; then
        salutation="Lieber $first_name"
    else
        salutation="Liebe $first_name"
    fi

    brief_content="Technische Berufsschule Zürich
Ausstellungsstrasse 70
8005 Zürich

Zürich, den $formatted_date

                        $first_name $last_name
                        $street $street_number
                        $postal_code $city
                        

$salutation

Es freut uns, Sie im neuen Schuljahr begrüssen zu dürfen.

Damit Sie am ersten Tag sich in unsere Systeme einloggen
können, erhalten Sie hier Ihre neue Emailadresse und Ihr
Initialpasswort, das Sie beim ersten Login wechseln müssen.

Emailadresse:   $email
Passwort:       $password


Mit freundlichen Grüssen

IhrVorname IhrNachname
(TBZ-IT-Service)


admin.it@tbz.ch, Abt. IT: +41 44 446 96 60"


    echo "$brief_content" > "${email}.brf"

done < mock_data.csv


zip -r "$archive_name" "$output_file" *.brf

rm *.brf

echo "Verarbeitung abgeschlossen. Ergebnisse wurden in $output_file gespeichert und Briefe erstellt."
echo "Archiv-Datei $archive_name wurde erstellt."
