#!/bin/bash

# FTP-Zugangsdaten
FTP_SERVER="ftp.haraldmueller.ch"
FTP_USER="schueler"
FTP_PASSWORD="studentenpasswort"
FTP_DIR="/mnt/c/Users/TBZ/M122"
HTML_FILE="index.html"

# E-Mail-Einstellungen
EMAIL_TO="lian.keller@edu.tbz.ch"
EMAIL_SUBJECT="Systeminfo"

# Schwellwerte
DISK_THRESHOLD=80  
MEMORY_THRESHOLD=80  

# Funktion zum Sammeln der Systeminformationen
get_system_info() {
  uptime=$(uptime -p)
  current_time=$(date '+%Y-%m-%d %H:%M:%S')
  disk_usage=$(df -h / | awk 'NR==2 {print $5}' | sed 's/%//')
  disk_free=$(df -h / | awk 'NR==2 {print $4}')
  hostname=$(hostname)
  ip_address=$(hostname -I | awk '{print $1}')
  os_name=$(uname -s)
  os_version=$(uname -r)
  cpu_model=$(lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^ *//g')
  cpu_cores=$(lscpu | grep "^CPU(s):" | awk -F: '{print $2}' | sed 's/^ *//g')
  total_memory=$(free -h | grep "Mem:" | awk '{print $2}')
  used_memory=$(free -h | grep "Mem:" | awk '{print $3}')
  free_memory=$(free -h | grep "Mem:" | awk '{print $4}')
  memory_usage=$(free | grep "Mem:" | awk '{printf("%.0f", $3/$2 * 100.0)}')

  # Disk-Ampel
  if [ $disk_usage -lt 60 ]; then
    disk_color="green"
  elif [ $disk_usage -lt 80 ]; then
    disk_color="yellow"
  else
    disk_color="red"
  fi

  # Memory-Ampel
  if [ $memory_usage -lt 60 ]; then
    memory_color="green"
  elif [ $memory_usage -lt 80 ]; then
    memory_color="yellow"
  else
    memory_color="red"
  fi

  # HTML-Ausgabe
  echo "<html>
<head><title>System Monitoring</title></head>
<body>
<h1>System Monitoring</h1>
<table border=\"1\">
<tr><th>Parameter</th><th>Value</th><th>Status</th></tr>
<tr><td>Uptime</td><td>$uptime</td><td></td></tr>
<tr><td>Current Time</td><td>$current_time</td><td></td></tr>
<tr><td>Disk Usage</td><td>$disk_usage% used, $disk_free free</td><td style=\"background-color:$disk_color;\"></td></tr>
<tr><td>Hostname</td><td>$hostname</td><td></td></tr>
<tr><td>IP Address</td><td>$ip_address</td><td></td></tr>
<tr><td>OS Name</td><td>$os_name</td><td></td></tr>
<tr><td>OS Version</td><td>$os_version</td><td></td></tr>
<tr><td>CPU Model</td><td>$cpu_model</td><td></td></tr>
<tr><td>CPU Cores</td><td>$cpu_cores</td><td></td></tr>
<tr><td>Total Memory</td><td>$total_memory</td><td></td></tr>
<tr><td>Used Memory</td><td>$used_memory</td><td></td></tr>
<tr><td>Free Memory</td><td>$free_memory</td><td style=\"background-color:$memory_color;\"></td></tr>
</table>
</body>
</html>"
}

output_file=""
while getopts "f" opt; do
  case $opt in
    f)
      output_file="/home/lian/log/"$(date '+%Y-%m')-sys-$(hostname).log
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

system_info=$(get_system_info)

if [ -n "$output_file" ]; then
  echo "$system_info" >> "$output_file"
fi

# HTML-Datei erstellen
echo "$system_info" > "$HTML_FILE"

# HTML-Datei auf den FTP-Server hochladen
ftp -inv $FTP_SERVER <<EOF
user $FTP_USER $FTP_PASSWORD
cd lian
put $HTML_FILE
cd ..
bye
EOF

# E-Mail-Benachrichtigung senden, wenn Schwellwerte überschritten werden
if [ $disk_usage -ge $DISK_THRESHOLD ] || [ $memory_usage -ge $MEMORY_THRESHOLD ]; then
  email_body="Achtung,\n\nDer folgende Teil des Systems ist überlastet:\n"
  if [ $disk_usage -ge $DISK_THRESHOLD ]; then
    email_body+="\nDisk Usage: $disk_usage% (Threshold: $DISK_THRESHOLD%)"
  fi
  if [ $memory_usage -ge $MEMORY_THRESHOLD ]; then
    email_body+="\nMemory Usage: $memory_usage% (Threshold: $MEMORY_THRESHOLD%)"
  fi
  email_body+="\n\nBitte unternehmen Sie nötige Maßnahmen.\n\nLiebe Grüße,\nSystem Monitoring Script"
  echo -e "$email_body" | mail -s "$EMAIL_SUBJECT" "$EMAIL_TO"
fi
